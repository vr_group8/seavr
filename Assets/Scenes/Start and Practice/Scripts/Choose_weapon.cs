﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Choose_weapon : MonoBehaviour
{
    public GameObject GunPrefab;
    public GameObject SwordPrefab;
    public GameObject cur_obj;
    public GameObject GunParent;
    public GameObject SwordParent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Choose_Gun(){
        Destroy(cur_obj);
        cur_obj = Instantiate(GunPrefab, GunParent.transform);
    }
    public void Choose_Sword(){
        Destroy(cur_obj);
        cur_obj = Instantiate(SwordPrefab, SwordParent.transform);
    }

}
