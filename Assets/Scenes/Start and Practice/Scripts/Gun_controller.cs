﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using UnityEngine.UI;

[RequireComponent(typeof(XRGrabInteractable))]
public class Gun_controller : MonoBehaviour
{
    XRGrabInteractable m_InteractableBase;
    //[SerializeField] ParticleSystem m_BubbleParticleSystem = null;

    //public InputActionReference SecondaryButtonReference = null;
    public GameObject BulletPrefab;
    public GameObject BulletParent;
    public bool isheld;
    //public Text bulletui;
    //int bulletcount;
    const string k_AnimTriggerDown = "TriggerDown";
    const string k_AnimTriggerUp = "TriggerUp";
    const float k_HeldThreshold = 0.01f;

    float m_TriggerHeldTime;
    bool m_TriggerDown;
    //float shoot_time;
    bool not_shoot = true;
    //bool Fire_mode = true;   // true for fire, false for bullet

    // Start is called before the first frame update
    void Start()
    {
        m_InteractableBase = GetComponent<XRGrabInteractable>();
        m_InteractableBase.selectExited.AddListener(DroppedGun);
        m_InteractableBase.activated.AddListener(TriggerPulled);
        m_InteractableBase.deactivated.AddListener(TriggerReleased);
        //bulletcount = 20;
        //SecondaryButtonReference.action.started += Toggle;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_TriggerDown)
        {
            m_TriggerHeldTime += Time.deltaTime;
            if(not_shoot){
                Instantiate(BulletPrefab, BulletParent.transform.position, BulletParent.transform.rotation);
                //bulletcount--;
                //bulletui.text = "" + bulletcount;
                //not_shoot = false;
            }
        }

        
    }

    void Toggle(InputAction.CallbackContext context){
        // bool new_mode = !Fire_mode;
        // Fire_mode = new_mode;
        //bulletcount = 20;
        //bulletui.text = "" + bulletcount;
        //if(GameObject.Find("Tutorial").GetComponent<Tutorial>().current_tuto == 9){
            //Tutorial tmp =  GameObject.Find("Tutorial").GetComponent<Tutorial>();
            //tmp.current_tuto = 10;
            //print("tuto 10");
            //tmp.tutorials[9].transform.position -= new Vector3(0, 500, 0);
            //tmp.tutorials[10].transform.position += new Vector3(0, 500, 0);
        //}
        //print("reset bullet");
    }

    void TriggerReleased(DeactivateEventArgs args)
    {
        not_shoot = true;
        m_TriggerDown = false;
        m_TriggerHeldTime = 0f;
        //m_BubbleParticleSystem.Stop();
    }

    void TriggerPulled(ActivateEventArgs args)
    {
        m_TriggerDown = true;
    }

    void DroppedGun(SelectExitEventArgs args)
    {
        // In case the gun is dropped while in use.

        m_TriggerDown = false;
        m_TriggerHeldTime = 0f;
        //m_BubbleParticleSystem.Stop();
    }

    public void ShootEvent()
    {
        //m_BubbleParticleSystem.Emit(1);
    }

    public void holdgun(){
        isheld = true;
        print("hold gun");
    }

    public void releasegun(){
        isheld = false;
    }

}
