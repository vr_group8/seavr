﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class TeleportController : MonoBehaviour
{
    public GameObject BaseControllerGameObject;
    public GameObject TeleportationControllerGameObject;

    public InputActionReference teleportActivationReference;
    [Space]

    public UnityEvent onTeleportActivate;
    public UnityEvent onTeleportCancel;
    // Start is called before the first frame update
    void Start()
    {
        teleportActivationReference.action.performed += TeleportModeActive;
        teleportActivationReference.action.canceled += TeleportModeCancel;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void TeleportModeCancel(InputAction.CallbackContext obj){
        Invoke("DeactivateTeleporter", 0.1f);
    }

    void DeactivateTeleporter() => onTeleportCancel.Invoke();

    void TeleportModeActive(InputAction.CallbackContext obj){
        onTeleportActivate.Invoke();
    }
}
