﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;


public class Tutorial : MonoBehaviour
{
    public int current_tuto = 1;
    public InputActionReference PrimaryButtonReference = null;
    public InputActionReference PrimaryButtonReference2 = null;
    public GameObject[] tutorials;
    bool pressed = false;
    public UnityEvent OnPress;
    public Text bulletui;
    int bulletcount;
    // Start is called before the first frame update
    void Start()
    {
        //tutorials[1].transform.position = new Vector3(0, 500, 0);
        for(int i = 2; i < tutorials.Length; i++){
            tutorials[i].transform.position -= new Vector3(0, 500, 0);
        }
        bulletcount = 20;
    }

    // Update is called once per frame
    void Update()
    {
        float test = PrimaryButtonReference.action.ReadValue<float>();
        float enter = PrimaryButtonReference2.action.ReadValue<float>();
        if(test > 0.0 && !pressed){
            print("primary button is " + test);
            OnPress.Invoke();
            pressed = true;
        }
        else if(test <= 0.0 && pressed){
            pressed = false;
            print("release");
        }
        
        if(enter > 0.0){
            SceneManager.LoadScene("MyWaveScene");
        }
    }

    public void Toggle(InputAction.CallbackContext context){
        if(current_tuto == 1){
            current_tuto = 2;
            tutorials[1].transform.position -= new Vector3(0, 500, 0);
            tutorials[2].transform.position += new Vector3(0, 500, 0);
        }
        else if(current_tuto == 2){
            current_tuto = 3;
            tutorials[2].transform.position -= new Vector3(0, 500, 0);
            tutorials[3].transform.position += new Vector3(0, 500, 0);
        }
    }

    public void test(){
        if(current_tuto == 1){
            current_tuto = 2;
            tutorials[1].transform.position -= new Vector3(0, 500, 0);
            tutorials[2].transform.position += new Vector3(0, 500, 0);
        }
        else if(current_tuto == 2){
            current_tuto = 3;
            tutorials[2].transform.position -= new Vector3(0, 500, 0);
            tutorials[3].transform.position += new Vector3(0, 500, 0);
        }
        else if(current_tuto == 3){
            current_tuto = 4;
            tutorials[3].transform.position -= new Vector3(0, 500, 0);
            tutorials[4].transform.position += new Vector3(0, 500, 0);
        }
        else if(current_tuto == 4){
            current_tuto = 5;
            tutorials[4].transform.position -= new Vector3(0, 500, 0);
            tutorials[5].transform.position += new Vector3(0, 500, 0);
        }
        else if(current_tuto == 5){
            current_tuto = 6;
            tutorials[5].transform.position -= new Vector3(0, 500, 0);
            tutorials[6].transform.position += new Vector3(0, 500, 0);
        }
        // else if(current_tuto == 6){
        //     current_tuto = 7;
        //     tutorials[6].transform.position -= new Vector3(0, 500, 0);
        //     tutorials[7].transform.position += new Vector3(0, 500, 0);
        // }
        else if(current_tuto == 7){
            current_tuto = 8;
            tutorials[7].transform.position -= new Vector3(0, 500, 0);
            tutorials[8].transform.position += new Vector3(0, 500, 0);
        }
        // else if(current_tuto == 8){
        //     current_tuto = 9;
        //     tutorials[8].transform.position -= new Vector3(0, 500, 0);
        //     tutorials[9].transform.position += new Vector3(0, 500, 0);
        // }
        // else if(current_tuto == 9){
        //     current_tuto = 10;
        //     tutorials[9].transform.position -= new Vector3(0, 500, 0);
        //     tutorials[10].transform.position += new Vector3(0, 500, 0);
        // }
        else if(current_tuto == 10){
            SceneManager.LoadScene("MyWaveScene");
        }
        // else if(current_tuto == 11){
        //     current_tuto = 12;
        //     tutorials[11].transform.position -= new Vector3(0, 500, 0);
        //     tutorials[12].transform.position += new Vector3(0, 500, 0);
        // }
    }

    public void changetuto67(){
        if(current_tuto == 6){
            current_tuto = 7;
            tutorials[6].transform.position -= new Vector3(0, 500, 0);
            tutorials[7].transform.position += new Vector3(0, 500, 0);
        }
    }

    public void changetuto89(){
        if(current_tuto == 8){
            current_tuto = 9;
            bulletui.text = "20";
            tutorials[8].transform.position -= new Vector3(0, 500, 0);
            tutorials[9].transform.position += new Vector3(0, 500, 0);
        }
    }
}
