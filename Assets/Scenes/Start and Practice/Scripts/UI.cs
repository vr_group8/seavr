﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class UI : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject rig1;
    public GameObject rig2;
    public Button start;
    //public Button history;
    public Button setting;
    public Slider volume_slider;
    public Image audio_on;
    public Image audio_prog;
    public Image audio_bg;
    public Button leave;
    public AudioClip buttonpress;
    public AudioSource source;
    void Start()
    {
        volume_slider.transform.position += new Vector3(0, -100, 0);
        audio_on.transform.position += new Vector3(0, -100, 0);
        audio_prog.transform.position += new Vector3(0, -100, 0);
        audio_bg.transform.position += new Vector3(0, -100, 0);
        leave.transform.position += new Vector3(0, -100, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changescene(){
        print("active xr origin2");
        rig1.SetActive(false);
        rig2.SetActive(true);
    }
    
    public void scene1(){
        SceneManager.LoadScene("Scene1");
        print("select");
    }
    
    public void scene2(){
        SceneManager.LoadScene("Scene2");
        print("select");
    }
    
    public void scene3(){
        SceneManager.LoadScene("Scene3");
        print("select");
    }

    public void changevolume(){
        audio_prog.fillAmount = volume_slider.value;
        source.volume = volume_slider.value;
    }

    public void showbuttons(){
        start.transform.position += new Vector3(0, 100, 0);
        //history.transform.position += new Vector3(0, 100, 0);
        setting.transform.position += new Vector3(0, 100, 0);
        volume_slider.transform.position += new Vector3(0, -100, 0);
        audio_on.transform.position += new Vector3(0, -100, 0);
        audio_prog.transform.position += new Vector3(0, -100, 0);
        audio_bg.transform.position += new Vector3(0, -100, 0);
        leave.transform.position += new Vector3(0, -100, 0);
    }

    public void disbuttons(){ 
        start.transform.position += new Vector3(0, -100, 0);
        //history.transform.position += new Vector3(0, -100, 0);
        setting.transform.position += new Vector3(0, -100, 0);
        volume_slider.transform.position += new Vector3(0, 100, 0);
        audio_on.transform.position += new Vector3(0, 100, 0);
        audio_prog.transform.position += new Vector3(0, 100, 0);
        audio_bg.transform.position += new Vector3(0, 100, 0);
        leave.transform.position += new Vector3(0, 100, 0);
    }

    public void playbutton(){
        source.PlayOneShot(buttonpress);
    }

}
