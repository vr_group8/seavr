using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class XtoChangeScene : MonoBehaviour
{
    public InputActionReference PrimaryButtonReference;
    public GameObject XROrigin2;

    void Awake(){
        PrimaryButtonReference.action.started += X_Pressed;
    }

    void OnDestroy(){
        PrimaryButtonReference.action.started -= X_Pressed;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void X_Pressed(InputAction.CallbackContext ctx){
        if(XROrigin2.activeSelf)
            SceneManager.LoadScene("Scene1");
    }
}
