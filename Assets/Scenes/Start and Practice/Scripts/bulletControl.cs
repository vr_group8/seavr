﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletControl : MonoBehaviour
{
    bool flag = false;
    public GameObject Target;
    public target target_score;
   
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * 5f;
        Target = GameObject.Find("Target");
        target_score = Target.GetComponent<target>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision other){
        if(other.gameObject.name == "collider"){
            //flag = true;
            Vector3 pos_diff = Target.transform.position - transform.position;
            float dis_2 = pos_diff.x*pos_diff.x + pos_diff.y*pos_diff.y + pos_diff.z*pos_diff.z;
            if(dis_2 <= 0.010f){
                target_score.score += 50;
            }
            else if(dis_2 <= 0.0313f){
                target_score.score += 25;
            }
            else if(dis_2 <= 0.0747f){
                target_score.score += 20;
            }
            else if(dis_2 <= 0.1483f){
                target_score.score += 15;
            }
            else if(dis_2 <= 0.2475f){
                target_score.score += 10;
            }
            print("hit");
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
            // if(GameObject.Find("Tutorial").GetComponent<Tutorial>().current_tuto == 4){
            //     Tutorial tmp =  GameObject.Find("Tutorial").GetComponent<Tutorial>();
            //     tmp.current_tuto = 5;
            //     tmp.tutorials[4].transform.position -= new Vector3(0, 500, 0);
            //     tmp.tutorials[5].transform.position += new Vector3(0, 500, 0);
            // }
            Destroy(gameObject , 1);
        }
        else if(other.gameObject.name == "Target"){
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
            Destroy(gameObject , 1);
        }
    }




}
