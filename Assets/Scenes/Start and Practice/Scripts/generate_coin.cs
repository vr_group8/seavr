﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generate_coin : MonoBehaviour
{
    public GameObject CoinPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void New_coin(){
        Instantiate(CoinPrefab, transform.position+new Vector3(0, -1.429f, -0.02f), transform.rotation);

    }
}
