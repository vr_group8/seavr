using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatControl : MonoBehaviour
{
    public float forward_speed = 0.1f;
    public float y_offset = 0f;
    public float y_height = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(y_height == 0f)
            transform.position += (Vector3.forward) * forward_speed * Time.deltaTime;
        else{
            if(transform.position.y < y_height){
                transform.position += (Vector3.forward + new Vector3(0,y_offset,0)) * forward_speed * Time.deltaTime;
            }
            else{
                transform.position += (Vector3.forward) * forward_speed * Time.deltaTime;
            }
        }
    }
}
