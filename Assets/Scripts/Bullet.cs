using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("WaitAndDestroy");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collider){
        if(collider.transform.tag == "Robot"){
            // collider.gameObject.GetComponent<Robot>().gotShoot();
             Destroy(gameObject);
        }
        // else{
        //     Destroy(gameObject);
        // }
    }

    IEnumerator WaitAndDestroy()
    {
        yield return new WaitForSeconds(10);
        Destroy(gameObject);
    }
}
