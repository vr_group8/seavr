﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class HandPresence : MonoBehaviour
{
    public InputActionReference Trigger;
    public InputActionReference Grip;

    private GameObject spawnedHandModel;
    public Animator handAnimator;
    public string anim_trigger_name = "Pinch";
    public string anim_grip_name = "Flex";
    private int triggerHash;
    private int gripHash;
    
    void Awake(){
        // Trigger.action.started += Triggered;
    }

    void OnDestroy(){
        // Trigger.action.started -= Triggered;
    }
    void Start()
    {
        // check_devices();

        // spawnedHandModel = Instantiate(handModel, transform);
        // handAnimator = GetComponent<Animator>();
        triggerHash = Animator.StringToHash(anim_trigger_name);
        gripHash = Animator.StringToHash(anim_grip_name);

        // handAnimator.SetFloat(triggerHash, 0.5f);
        // handAnimator.SetFloat(gripHash, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHandAnimation();
    }

    void UpdateHandAnimation(){
        float trigger = Trigger.action.ReadValue<float>();
        float grip = Grip.action.ReadValue<float>();

        handAnimator.SetFloat(triggerHash, trigger);
        handAnimator.SetFloat(gripHash, grip);

    }

    void check_devices(){
        List<UnityEngine.XR.InputDevice> devices = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevices(devices);
        foreach (var item in devices){
            Debug.Log(item.name + item.characteristics);
        }
    }
}
