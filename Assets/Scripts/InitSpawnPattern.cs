using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitSpawnPattern : MonoBehaviour
{
    public float show_up_time = 5.0f;
    public float robot_speed_up = 0f;
    public List<RobotSpawnPoint.RobotPattern> robotPatterns = new List<RobotSpawnPoint.RobotPattern>();
    private float spawn_time = 0f;
    private FileStream fs;
    private StreamWriter stream;
    public string csv_path = "Assets/Wiibuddy/Script/Robot_time.csv";
    // Start is called before the first frame update
    void Start()
    {
        
        string time_start = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();
        csv_path = "Assets/Wiibuddy/Script/Robot_time_Scene" + SceneManager.GetActiveScene().buildIndex.ToString() + "_" + time_start + ".csv";
        fs = new FileStream(csv_path, FileMode.Append , FileAccess.Write);
        stream =  new StreamWriter(fs);

        int index = 0;
        foreach(Transform c in transform){
        c.GetComponent<MeshRenderer>().enabled = false;
            var rsp = c.gameObject.GetComponent<RobotSpawnPoint>();
            if(index > robotPatterns.Count - 1){
                rsp.enabled = false;
                continue;
            }
            rsp.spawn_interval = spawn_time;
            rsp.showup_interval = show_up_time;
            rsp.spawn_when_start = false;
            rsp.spawn_times = 1;
            spawn_time += 5f;
            rsp.robotPattern = robotPatterns[index];
            rsp.staytime = 6f;
            rsp.stream = stream;
            // rsp.robot_speed = 1f + robot_speed_up;
            index++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy(){        
        stream.Close();
        stream.Dispose();
    }
}
