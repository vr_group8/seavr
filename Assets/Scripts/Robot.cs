using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{
    public int HP { get; set; }
    public int maxHP { get; set; }

    void Start()
    {
        Debug.Log("I am robot");
    }

    void Update()
    {
        
    }

    public virtual void gotShoot(){
        Debug.Log("Robot got shoot");
    }
}
