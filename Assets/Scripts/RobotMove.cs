﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System;
 

public class RobotMove : Robot
{
    // public GameObject XROriginPoint;
    public float forward_speed = 1.0f;
    public Transform grid_9;
    // public ParticleSystem partical;
    private Transform gridTransform;
    private Vector3 dir;
    // record which cube index our pattern is going to move
    private int nodeIndex;
    private bool end = false;
    private bool stay = false;
    public float staytime = 0.0f;
    public int headtoIndex = 0;
    [Range(0f,1f)]
    public float rotateProbability;
    public float rotateAngle = 10f;
    private List<int> pattern;    
    private bool exit = false;
    public StreamWriter stream;
    private MeshRenderer meshRenderer;
    public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        pattern = new List<int>();
        pattern.Add(headtoIndex);

        nodeIndex = 0;
        gridTransform = grid_9.GetChild(pattern[nodeIndex] - 1);
        StartCoroutine(RandomRotate());
        
        // meshRenderer = GetComponent<MeshRenderer>();
        // dir = caculateDir();
    }

    // Update is called once per frame
    void Update()
    {
        updatePosition();
    }

    public override void gotShoot(){
        // meshRenderer.enabled = false;
        StartCoroutine(explode());
    }

    private void OnTriggerEnter(Collider other) {
        // partical.Play(true);
        // if(other.tag == "bullet"){
        //     gotShoot();
        // }
        // Debug.Log("Trigger");
    }

    private void OnTriggerExit(Collider other) {
        // partical.Stop(true);        
    }

    public void set9GridTransform(Transform grid9){
        this.grid_9 = grid9;
    }
    // private Vector3 caculateDir(){
    //     gridPosition = grid_9.GetChild(pattern[nodeIndex]).position;
    //     print(gridPosition);
    //     Vector3 dir = (gridPosition - transform.position);
    //     // z is depth from boat
    //     // dir.z = 0; 
    //     return dir.normalized;
    // }

    private void updatePosition(){
        if(end) {
            if(exit){
                switch(headtoIndex){                            
                    case 7:
                    case 8:
                    case 9: 
                        // if(Mathf.Abs(transform.position.y - grid_9.GetChild(13).position.y) < 0.01f){
                        //     Destroy(gameObject);
                        // }
                        // else{
                        //     //use grid_9 since robot will random rotate
                        //     transform.position += grid_9.up * -1 * forward_speed * Time.deltaTime;
                        // }
                        // break;
                    default: // 123456
                        if(Mathf.Abs(transform.position.y - grid_9.GetChild(10).position.y) < 0.01f){
                            Destroy(gameObject);
                        }
                        else{
                            transform.position += grid_9.up * forward_speed * Time.deltaTime;
                        }
                        break;
                    
                }
                
            }
            return;
        }
        if(Vector3.Distance(transform.position, gridTransform.position) < 0.01f){
            if(nodeIndex == 0){
                transform.parent = grid_9;
            }
            nodeIndex++;

            if(nodeIndex >= pattern.Count){
                end = true;   
                StartCoroutine(exit_view_field());             
                string[] time_array = new string[] {pattern[nodeIndex-1].ToString()+": ", Time.time.ToString()};
                WriteCSV(time_array);
            }
            else{                
                if(pattern[nodeIndex] >= 10 && pattern[nodeIndex-1] >= 10){ //need to teleport to otherside
                    // print(pattern[nodeIndex]);
                    transform.position = grid_9.GetChild(pattern[nodeIndex] - 1).position;
                    nodeIndex++;
                }
                gridTransform = grid_9.GetChild(pattern[nodeIndex] - 1);
                // print(nodeIndex);
                // if(staytime > 0f)
                //     StartCoroutine(stayTime());

            }
        }
        else if(!stay){
            transform.position = Vector3.MoveTowards(transform.position, gridTransform.position,  forward_speed * Time.deltaTime);
        }
        
    }

    IEnumerator exit_view_field(){
        yield return new WaitForSeconds(staytime);
        exit = true;
    }

    IEnumerator stayTime(){
        stay = true;
        yield return new WaitForSeconds(staytime);
        stay = false;
    }

    IEnumerator RandomRotate(){
        while(true){            
            if(UnityEngine.Random.Range(0f,1f) < rotateProbability){
                // print("Rotate");
                // change_rotation_smooth(new Vector3(Random.Range(-rotateAngle,rotateAngle),Random.Range(-rotateAngle,rotateAngle),Random.Range(-rotateAngle,rotateAngle)));
                transform.Rotate(UnityEngine.Random.Range(-rotateAngle,rotateAngle),UnityEngine.Random.Range(-rotateAngle,rotateAngle),UnityEngine.Random.Range(-rotateAngle,rotateAngle));
            }
            else{                
                transform.LookAt(grid_9.parent);
            }
            yield return new WaitForSeconds(1f);
        }
    }
    
    void change_rotation_smooth(Vector3 eulerAngles){
        Quaternion originalRotation = transform.rotation;
        transform.Rotate(eulerAngles);
        Quaternion targetRotation = Quaternion.Euler(eulerAngles);
    }

    public void WriteCSV(string[] strs){
        stream.WriteLine($"{strs[0]} , {strs[1]} , {"\n"}");
    }

    IEnumerator explode(){
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        // Debug.Log("explode");
        yield return new WaitForSeconds(1.0f);
        Destroy(gameObject);
    }
}
