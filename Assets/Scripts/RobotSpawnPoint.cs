﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RobotSpawnPoint : MonoBehaviour
{
    public GameObject Robot;
    public Transform grid_9;
    [HeaderAttribute("Spawn Control")]
    public float showup_interval = 5.0f;
    public float spawn_interval = 1.0f;
    public bool spawn_when_start = false;
    public bool INF_spawn = true;
    public int spawn_times = 1;
    public enum RobotPattern{
        _123,
        _321,
        _456,
        _654,
        _789,
        _987,
        _13,
        _31,
        _46,
        _64,
        _79,
        _97,
        _159,
        _357,
        _000
    }
    [HeaderAttribute("Robot Pattern")]
    public float robot_speed = 1.0f;
    public RobotPattern robotPattern = RobotPattern._123;
    public float staytime = 0.0f;

    private GameObject xrorigin;
    public StreamWriter stream;
    
    // [System.Serializable]
    // public struct CubePattern{
    //     [Range(1,9)]
    //     public int CubeIndex;
    //     public float staytime;

    // }
    // public List<CubePattern> movePattern;
    // Start is called before the first frame update
    void Start()
    {
        xrorigin = GameObject.FindGameObjectWithTag("xrorigin");
        if(xrorigin != null){
            grid_9 = xrorigin.GetComponent<Basic2dRefPattern>().grid9;
            robot_speed = xrorigin.transform.parent.GetComponent<BoatControl>().forward_speed;
        }
        else{
            Debug.LogError("xrorigin not found");
        }
        GetComponent<MeshRenderer>().enabled = false;
        if(INF_spawn) {
            spawn_times = -1;
        }
        // StartCoroutine(SpawnMonster());
    }

    // Update is called once per frame
    void Update()
    {
        if(isXROriginClose() && spawn_times > 0){
            // print("spawn: " + Time.time.ToString());
            spawn_times--;
            spawnRobot();
        }
    }

    IEnumerator SpawnMonster()
    {   
        if(spawn_when_start){
            spawnRobot();
            spawn_times--;
        }
        while(INF_spawn || spawn_times > 0){
            yield return new WaitForSeconds(spawn_interval);
            spawnRobot();
            spawn_times--;
        }        
    }

    void spawnRobot(){
         switch(robotPattern){
            case RobotPattern._123:
                spawn(1);
                randomSpawmSecond();
                spawn(2);
                randomSpawmSecond();
                spawn(3);
                break;
            case RobotPattern._321:
                spawn(3);
                randomSpawmSecond();
                spawn(2);
                randomSpawmSecond();
                spawn(1);
                break;
            case RobotPattern._456:
                spawn(4);
                randomSpawmSecond();
                spawn(5);
                randomSpawmSecond();
                spawn(6);
                break;
            case RobotPattern._654:
                spawn(6);
                randomSpawmSecond();
                spawn(5);
                randomSpawmSecond();
                spawn(4);
                break;
            case RobotPattern._789:
                spawn(7);
                randomSpawmSecond();
                spawn(8);
                randomSpawmSecond();
                spawn(9);
                break;
            case RobotPattern._987:
                spawn(9);
                randomSpawmSecond();
                spawn(8);
                randomSpawmSecond();
                spawn(7);
                break;
            case RobotPattern._13:
                spawn(1);
                randomSpawmSecond();
                spawn(3);
                break;
            case RobotPattern._31:
                spawn(3);
                randomSpawmSecond();
                spawn(1);
                break;
            case RobotPattern._46:
                spawn(4);
                randomSpawmSecond();
                spawn(6);
                break;
            case RobotPattern._64:
                spawn(6);
                randomSpawmSecond();
                spawn(4);
                break;
            case RobotPattern._79:
                spawn(7);
                randomSpawmSecond();
                spawn(9);
                break;
            case RobotPattern._97:
                spawn(9);
                randomSpawmSecond();
                spawn(7);
                break;
            case RobotPattern._159:
                spawn(1);
                randomSpawmSecond();
                spawn(5);
                randomSpawmSecond();
                spawn(9);
                break;
            case RobotPattern._357:
                spawn(3);
                randomSpawmSecond();
                spawn(5);
                randomSpawmSecond();
                spawn(7);
                break;
            case RobotPattern._000:
                break;
        }
    }

    void spawn(int Index){
        
        GameObject robot = Instantiate(Robot, transform);;
        robot.transform.parent = grid_9;
        robot.transform.localScale  = new Vector3(0.5f,0.5f,0.5f);
        RobotMove r = robot.GetComponent<RobotMove>();
        r.set9GridTransform(grid_9);
        r.headtoIndex = Index;
        r.staytime = staytime;
        r.forward_speed = robot_speed;
        r.stream = stream;
        
        robot.SetActive(true);
    }

    private bool isXROriginClose() {
        float sqr_distance = (xrorigin.transform.position - transform.position).sqrMagnitude;
        float detect_distance = showup_interval * robot_speed;
        if(sqr_distance < detect_distance * detect_distance){
            return true;
        }
        else{
            return false;
        }
    }

    IEnumerator randomSpawmSecond(){

        yield return new WaitForSeconds(10f);
    }
}
