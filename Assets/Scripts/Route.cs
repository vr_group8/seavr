using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : MonoBehaviour
{
    public GameObject Boat;
    public float routeSpeed = 1f;
    public float turnSpeed = 0.1f;
    public float lengthToStartRotate = 10f;
    public float rotate_offset_y = 0f;
    private int toPointIndex = 0;
    private Transform toPoint;
    private Vector3 StartBoatPosition;
    private float timeCount = 0f;
    private Quaternion target_rotation;
    private Quaternion now_rotation;
    // private Vector3 toPositioninBoatHeight;
    private bool rotate_lock = false;
    // Start is called before the first frame update
    void Start()
    {
        if(Boat == null){
            Boat = GameObject.Find("raftnewComplete");
        } 

        Vector3 tmp;
        foreach(Transform child in transform){
            tmp = child.position;
            tmp.y = Boat.transform.position.y;
            child.position = tmp; 
            foreach(Transform c in child){
                c.gameObject.SetActive(false);
            }
        }
        // print(transform.childCount);
        toPoint = transform.GetChild(toPointIndex);
        StartBoatPosition = Boat.transform.position;
        setTargetRotation();
    }

    // Update is called once per frame
    void Update()
    {
        // toPositioninBoatHeight = new Vector3(toPoint.position.x, Boat.transform.position.y, toPoint.position.z);

        if(toPointIndex + 1 < transform.childCount && checkArrivedPoint(toPoint)){
                toPointIndex = toPointIndex + 1;
                print(toPointIndex);
                toPoint = transform.GetChild(toPointIndex);
                setTargetRotation();
        }
        if(toPointIndex + 1 <= transform.childCount){
            Boat.transform.position = Vector3.MoveTowards(Boat.transform.position, transform.GetChild(toPointIndex).position, routeSpeed * Time.deltaTime);
        }
        // if((Boat.transform.position - transform.GetChild(toPointIndex).position).magnitude < lengthToStartRotate){ 
            // updateNowRotation();
            // rotate_lock = true;
            // Vector3 relativePos = toPositioninBoatHeight - Boat.transform.position;
            // Boat.transform.rotation = Quaternion.Lerp(Boat.transform.rotation, now_rotation, timeCount * turnSpeed);
            // Boat.transform.rotation = now_rotation;
            // Boat.transform.LookAt(toPoint.position);
            // Vector3 tmp = new Vector3(toPoint.position.x, Boat.transform.position.y, toPoint.position.z);
            // Boat.transform.rotation = Quaternion.LookRotation(tmp);
            // Boat.transform.Rotate(0, Quaternion.LookRotation(toPoint.position).eulerAngles.y, 0);
            timeCount = timeCount + Time.deltaTime;
        // }
        // else{
            timeCount = 0f;
        // }
        if(Boat.transform.rotation == now_rotation){
            // rotate_lock = false;
        }

    }

    bool checkArrivedPoint(Transform to){
        if(to.position.x == Boat.transform.position.x && to.position.z == Boat.transform.position.z){
            return true;
        }
        return false;
    }

    void setTargetRotation(){
        print(toPoint.position);
        Vector3 tmp = new Vector3(toPoint.position.x, Boat.transform.position.y, toPoint.position.z);
        if(toPointIndex == 0){
            // target_rotation = Quaternion.FromToRotation(toPoint.position - StartBoatPosition, transform.GetChild(1).position - toPoint.position);
            
            target_rotation = Quaternion.LookRotation(tmp);
            target_rotation = Quaternion.Euler(Boat.transform.rotation.eulerAngles.x, target_rotation.eulerAngles.y, Boat.transform.rotation.eulerAngles.z);
        }
        else{
            // target_rotation = Quaternion.FromToRotation(toPoint.position - transform.GetChild(toPointIndex - 1).position, transform.GetChild(toPointIndex + 1).position - toPoint.position);
            target_rotation = Quaternion.LookRotation(tmp);            
            target_rotation = Quaternion.Euler(Boat.transform.rotation.eulerAngles.x, target_rotation.eulerAngles.y, Boat.transform.rotation.eulerAngles.z);
        }
        print(target_rotation.eulerAngles.y);

        // target_rotation = Quaternion.Euler(target_rotation.eulerAngles.x, target_rotation.eulerAngles.y + rotate_offset_y, target_rotation.eulerAngles.z);
    }
    void updateNowRotation(){
        if(rotate_lock == false){
            now_rotation = target_rotation;
        }
    }
}
