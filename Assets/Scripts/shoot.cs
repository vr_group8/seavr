﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class shoot : MonoBehaviour
{
    public InputActionReference Trigger;
    public float bullet_velocity = 0.01f;
    public GameObject Bullet;
    public Transform ShootPoint;
    public Vector3 shoot_direction_offset;
    // public ParticleSystem bang;
    private bool isGripped = false;
    private AudioSource audioPlayer;
    public GameObject Cube;
    public GameObject Cube2;
    private XRGrabInteractable XRgrab;
    public float bullet_size = 0.5f;
    public float bullet_offset = 1f;
    private GameObject xrorigin;

    void Awake(){
        Trigger.action.started += Triggered;
    }

    void OnDestroy(){
        Trigger.action.started -= Triggered;
    }
    void Start()
    {
        xrorigin = GameObject.FindGameObjectWithTag("xrorigin");
        var basic = xrorigin.GetComponent<Basic2dRefPattern>();
        audioPlayer = GetComponent<AudioSource>();
        if(xrorigin != null){
            // ShootPoint = basic.ShootPoint.transform;
        }
        else{
            Debug.LogError("xrorigin not found");
        }
        // anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Cube != null)
            Cube.transform.position = ShootPoint.position;
        if(Cube2 != null){
            Cube2.transform.position = transform.position;
        }
        // RaycastHit hit;
        // int layerMask = 1 << 13;
        // Does the ray intersect any objects excluding the player layer
        // if (Physics.Raycast(ShootPoint.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        // {
        //     Debug.DrawRay(ShootPoint.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        //     Debug.Log("Did Hit");
        // }
        // else
        // {
        //     Debug.DrawRay(ShootPoint.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
        //     Debug.Log("Did not Hit");
        // }
    }

    public void updateGrip(bool g){
        isGripped = g;
    }

    void Triggered(InputAction.CallbackContext ctx){
        if(!isGripped)
            return;
        // sp.transform.position += transform.forward * bullet_offset;
        // Quaternion rot = Quaternion.Euler(transform.rotation.eulerAngles.x-90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        GameObject b = Instantiate(Bullet, ShootPoint);
        b.transform.parent = xrorigin.transform.parent;
        b.transform.localScale = new Vector3(bullet_size/10f, bullet_size, bullet_size/10f);
        b.SetActive(true);
        Rigidbody rb = b.GetComponentInChildren<Rigidbody>();
        rb.velocity = bullet_velocity * (transform.forward );

        audioPlayer.Play();
        
        // anim.SetTrigger("Fire");
        // bang.Play(true);
        int layerMask = 1 << 13;
        RaycastHit hit;
        if(Physics.Raycast(ShootPoint.position, transform.forward, out hit, Mathf.Infinity, layerMask)){
            Debug.Log("tag is: ");
            Debug.Log(hit.collider.gameObject.tag);
            if(hit.collider.gameObject.tag == "Robot"){
                var comp = hit.collider.gameObject.GetComponent<RobotMove>();
                comp.gotShoot();
                Debug.Log("shoot");
            }
            else{
                Debug.Log(hit.collider.gameObject.tag);
            }
        }
    }

    public void Shoot(){

    }

void OnTriggerEnter(Collider other){
    print("trigger enter");
}
}
