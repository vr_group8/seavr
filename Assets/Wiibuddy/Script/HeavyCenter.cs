using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Text;
using System.IO;

public class HeavyCenter : MonoBehaviour
{
    // Start is called before the first frame update
    public float fixed_scale = 1;
    public float fixed_rotation = 180;
    public float per_second_rotate = 10;
    public WaterBoat boat_control;
    public GameObject boat;
    Rigidbody boat_rd;
    Transform boat_ts;
    float lerp_tm = 0f;
    public int now_scene = 1; // use to figure out which scene
    string[] number = {"x" , "y" , "z" , "w"};
    string time_start;
    void Start()
    {
        // boat_rd = boat.GetComponent<Rigidbody>();
        // boat_ts = boat.GetComponent<Transform>();
        time_start = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();
        now_scene = SceneManager.GetActiveScene().buildIndex;
        // print("now time is" + time_start);
    }

    // Update is called once per frame
    // add for commit
    void Update()
    {
        
    }

    public void graphic(float x , float y , float z , float w , float time){ //top right , left , bottom right , left
        // horizontal 150-15 = 135 vertical 300-15 = 285
        // 66 , x , 2.065972 , y , 0.4449732 , z , 2.34051 , w , 2.145726 , 
        x += 2.74f; y += 2.78f; z += 2.51f; w += 2.4f;
        // print(x + " and " + y + " and " + z + " and " + w);
        float weight = x + y + z + w;
        float posx = ((x*1 + y*(-1) + z*1 + w*(-1)) / weight);
        float posy = ((x*1 + y*1 + z*(-1) + w*(-1)) / weight);
        if(posx > 0 || posx < 0) {
            // boat_rd.AddForce(posx * fixed_scale , 0 , 0);
            // boat_control.steer = posx * (-1);
            // print("posx = " + posx);
            string[] balance_array = new string[] {time.ToString() , x.ToString() , y.ToString() , z.ToString() , w.ToString()};
            if(now_scene == 1){
                WriteCSV(balance_array , "Assets/Wiibuddy/Script/balance1_" + time_start + ".csv");
            }else if(now_scene == 2){
                WriteCSV(balance_array , "Assets/Wiibuddy/Script/balance2_" + time_start + ".csv");
            }else if(now_scene == 3){
                WriteCSV(balance_array , "Assets/Wiibuddy/Script/balance3_" + time_start + ".csv");
            }else if(now_scene == 0){
                WriteCSV(balance_array , "Assets/Wiibuddy/Script/balance0_" + time_start + ".csv");
            }
        }

    }

    // FileStream fs = new FileStream(filename, FileMode.Append, FileAccess.Write);
    public void WriteCSV(string[] strs , string path){
        if (!File.Exists(path))
        {
            File.Create(path).Dispose();
        }
        FileStream fs = new FileStream(path, FileMode.Append , FileAccess.Write);
        var stream =  new StreamWriter(fs);
        stream.WriteLine($"{strs[0]} , {number[0]} , {strs[1]} , {number[1]} , {strs[2]} , {number[2]} , {strs[3]} , {number[3]} , {strs[4]} , {"\n"}");
        stream.Close();
        stream.Dispose();
    }

    // void Rotate_Init(float posx){
    //     Quaternion raw_rotation = boat_ts.rotation;
    //     boat_ts.LookAt(new Vector3(0f , 0f , posx * fixed_rotation));
    //     Quaternion lookat_rotation = boat_ts.rotation;
    //     boat_ts.rotation = raw_rotation;
    //     float rotate_angle = Quaternion.Angle(raw_rotation , lookat_rotation);
    //     float lerp_speed = per_second_rotate / rotate_angle;
    //     boat.transform.rotation = Quaternion.Euler(0f , posx * fixed_rotation , 0f);
    //     Rotate_Func(lerp_speed , raw_rotation , lookat_rotation);
    // }

    // void Rotate_Func(float lerp_speed , Quaternion raw_rotation , Quaternion lookat_rotation){
    //     lerp_tm += Time.deltaTime * lerp_speed; 
    //     boat_ts.rotation = Quaternion.Lerp(raw_rotation, lookat_rotation, lerp_tm);
    //     if (lerp_tm >= 1) boat_ts.rotation = lookat_rotation;
    // }
    
}
